============
EE 198: HOPE
============
HOPE is a `DeCal <https://decal.berkeley.edu/>`_ taught by the IEEE student
branch at the University of California, Berkeley. HOPE is offered in two
different versions. Only one version is offered each semester.

`Hands-On Practical Electronics <hope.html>`_
=============================================
Introductory electronics for beginners and hobbyists

Last Offered: **Fall 2017**

`Hands-On PCB Engineering <pcb.html>`_
======================================
Real-world printed circuit board (PCB) design and layout for new and
intermediate board designers

Next Offered: **Fall 2018**
